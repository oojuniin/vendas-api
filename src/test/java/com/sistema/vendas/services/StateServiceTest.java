package com.sistema.vendas.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sistema.vendas.address.state.State;
import com.sistema.vendas.address.state.StateRepository;
import com.sistema.vendas.address.state.StateService;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 13/01/2022
 */

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
class StateServiceTest{

	@Autowired
	private StateRepository repository;

	@Autowired
	private StateService service;

	@BeforeAll
	void setup(){
		State ceara = new State(1L, "CE", "Ceara");
		State paraiba = new State(2L, "PB", "Paraiba");
		State maranhao = new State(3L, "MA", "Maranhão");

		service.save(ceara);
		service.save(paraiba);
		service.save(maranhao);
	}

	@Test
	@DisplayName("Buscando estado por ID")
	void case01(){
		Optional<State> state = service.findById(1L);
		assertEquals("Ceara", state.get()
															 .getName());
	}

	@Test
	@DisplayName("Buscando estado por UF")
	void case02(){
		Optional<State> state = service.findByUfIgnoreCase("PB");
		assertEquals("Paraiba", state.get()
																 .getName());
	}

	@Test
	@DisplayName("Buscando todos os estados")
	void case03(){
		List<State> staties = service.findAll();
		assertEquals(3, staties.size());
	}

	@Test
	@DisplayName("Buscando estado por nome")
	void case04(){
		List<State> state = service.findByNameStartingWithIgnoreCase("Ce");
		assertEquals("Ceara", state.get(0)
															 .getName());
	}

	@Test
	@DisplayName("Deletando um estado pela ID")
	void case05(){
		service.delete(1L);
		List<State> staties = service.findAll();
		assertEquals(2, staties.size());
	}

	@AfterAll
	void finish(){
		repository.deleteAll();
	}

}
