package com.sistema.vendas.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sistema.vendas.address.city.City;
import com.sistema.vendas.address.city.CityRepository;
import com.sistema.vendas.address.city.CityService;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 13/01/2022
 */

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
class CityServiceTest{

	@Autowired
	private CityRepository repository;

	@Autowired
	private CityService service;

	@BeforeAll
	void setup(){
		City pacatuba = new City(1L, "CE", "Pacatuba");
		City maracanau = new City(2L, "CE", "Maracanau");
		City campinaGrande = new City(3L, "PB", "Campina Grande");
		City joaoPessoa = new City(4L, "PB", "João Pessoa");

		service.save(pacatuba);
		service.save(maracanau);
		service.save(campinaGrande);
		service.save(joaoPessoa);
	}

	@Test
	@DisplayName("Buscando cidade por ID")
	void case01(){
		Optional<City> city = service.findById(1L);
		assertEquals("Pacatuba", city.get()
																 .getName());
	}

	@Test
	@DisplayName("Buscando cidade com ID nao existente")
	void case02(){
		Optional<City> city = service.findById(100L);
		assertEquals(Optional.empty(), city);
	}

	@Test
	@DisplayName("Buscando cidade pelo NOME")
	void case03(){
		List<City> cities = service.findByNameStartingWithIgnoreCase("Mara");
		assertEquals(1, cities.size());
		assertEquals("Maracanau", cities.get(0)
																		.getName());
	}

	@Test
	@DisplayName("Buscando uma cidade não existente")
	void case04(){
		List<City> cities = service.findByNameStartingWithIgnoreCase("XX");
		assertEquals(0, cities.size());
		assertTrue(cities.isEmpty());
	}

	@Test
	@DisplayName("Buscando cidade por UF")
	void case05(){
		List<City> cities = service.findByUfIgnoreCase("PB");
		assertEquals(2, cities.size());
		assertEquals("PB", cities.get(1)
														 .getUf());
	}

	@Test
	@DisplayName("Buscando uma cidade com Uf não existente")
	void case06(){
		List<City> cities = service.findByUfIgnoreCase("XX");
		assertEquals(0, cities.size());
		assertTrue(cities.isEmpty());
	}

	@Test
	@DisplayName("Buscando todas as cidades")
	void d(){
		List<City> cities = service.findAll();
		assertEquals(4, cities.size());
	}

	@Test
	@DisplayName("Deletando uma cidade pela ID")
	void e(){
		service.delete(1L);
		List<City> cities = service.findAll();
		assertEquals(3, cities.size());
	}

	@AfterAll
	void finish(){
		repository.deleteAll();
	}

}
