package com.sistema.vendas.shippingcompany;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.br.CNPJ;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sistema.vendas.address.Address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 09/12/2021
 */

@Data
@Entity(name = "carriers")
@NoArgsConstructor
@AllArgsConstructor
public class ShippingCompany implements Serializable{

	private static final long serialVersionUID = 1499174880472219939L;

	@Id
	@SequenceGenerator(name = "shipping_company_sequence", sequenceName = "shipping_company_sequence", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "shipping_company_sequence")
	@Column(updatable = false)
	private Long id;

	@NotEmpty(message = "Insira uma placa válida")
	@Size(max = 8)
	@Column(unique = true, nullable = false)
	private String board;

	@NotEmpty(message = "Insira um inscrição")
	@Size(max = 100)
	@Column(unique = true, nullable = false)
	private String enrollment;

	@NotEmpty(message = "Insira um nome")
	@Size(max = 150)
	@Column(nullable = false)
	private String name;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(nullable = false)
	private LocalDate registrationDate;

	@CNPJ
	@NotEmpty(message = "Insira um CNPJ válido")
	@Column(unique = true, nullable = false)
	@Size(max = 20)
	private String cnpj;

	@Email
	@NotEmpty(message = "Insira um email")
	@Column(unique = true)
	private String email;

	@Embedded
	@Valid
	@Cascade(CascadeType.ALL)
	private Address address;

	@NotEmpty(message = "Insira um número válido")
	@Column(unique = true, nullable = false)
	private String phoneNumberOne;

	private String phoneNumberTwo;

	private String comments;

}
