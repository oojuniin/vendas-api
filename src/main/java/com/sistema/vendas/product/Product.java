package com.sistema.vendas.product;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 18/12/2021
 */

@Data
@Entity(name = "products")
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable{

	private static final long serialVersionUID = -7107216404234725578L;

	@ApiModelProperty(value = "Id do produto")
	@Id
	@SequenceGenerator(name = "product_sequence", sequenceName = "product_sequence", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_sequence")
	@Column(updatable = false)
	private Long id;

	@NotEmpty(message = "Forneça um nome para o produto")
	@Column(unique = true, nullable = false)
	@Size(max = 50)
	private String name;

	@NotEmpty(message = "Forneça uma marca para o produto")
	@Column(nullable = false)
	@Size(max = 50)
	private String brand;

	@NotEmpty(message = "Forneça um departamento para o produto")
	@Column(nullable = false)
	@Size(max = 50)
	private String department;

	@NotEmpty(message = "Forneça um setor para o produto")
	@Column(nullable = false)
	@Size(max = 50)
	private String sector;

	@NotEmpty(message = "Forneça uma seção para o produto")
	@Column(nullable = false)
	@Size(max = 50)
	private String section;

	@NotEmpty(message = "Forneça um peso para o produto")
	@Column(nullable = false)
	@Size(max = 10)
	private String weight;

	@NotNull(message = "Forneça uma data de compra")
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(nullable = false)
	private LocalDate datePurchase;

	@NotNull(message = "Forneça uma quantidade")
	@Column(nullable = false)
	private Integer quantity;

	@NotNull(message = "Forneça a quantidade mínima")
	@Column(nullable = false)
	private Integer minimunQuantity;

	@NotNull(message = "Forneça um fornecedor")
	@Column(nullable = false, name = "has_a_supplier")
	private Boolean hasASupplier;

	@NotNull(message = "Forneça uma transportadora")
	@Column(nullable = false, name = "has_a_carrier")
	private Boolean hasACarrier;

	@NotNull(message = "Forneça o preço de custo")
	@Column(nullable = false)
	private BigDecimal costPrice;

	@NotNull(message = "Forneça o preço de venda")
	@Column(nullable = false)
	private BigDecimal sellingPrice;

}
