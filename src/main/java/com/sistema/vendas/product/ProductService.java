package com.sistema.vendas.product;

import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 18/12/2021
 */

public interface ProductService{

	List<Product> findAll();

	Optional<Product> findById(Long id);

	Product save(Product product);

	Product update(Product product);

	void delete(Long id);

}
