package com.sistema.vendas.product;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sistema.vendas.response.Response;
import com.sistema.vendas.utils.Utils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 18/12/2021
 */

@RestController
@RequestMapping("/api/v1/product")
@RequiredArgsConstructor
@Slf4j
public class ProductController{

	private final ProductService service;
	private static final String PRODUCT_RETRIEVED = "Product retrieved";

	@GetMapping("/find/all")
	public ResponseEntity<Response> findAll(){
		List<Product> products = service.findAll();
		return findProducts(products);
	}

	@GetMapping("/find/id/{id}")
	public ResponseEntity<Response> findById(@PathVariable("id") Long id){
		Optional<Product> product = service.findById(id);
		return findProduct(product);
	}

	@PostMapping("/save")
	public ResponseEntity<Response> save(@RequestBody ProductDTO dto){
		Product product = ProductDTO.convertToEntity(dto);
		log.info("Produto convertido com sucesso");
		Product save = service.save(product);
		ProductDTO productDTO = ProductDTO.convertToDto(save);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
																							.path("/api/v1/product/find/id/{id}")
																							.buildAndExpand(productDTO.getId())
																							.toUri();
		log.info("Produto salvo com sucesso");
		return ResponseEntity.created(location)
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(productDTO))
																			 .message(PRODUCT_RETRIEVED)
																			 .status(HttpStatus.ACCEPTED)
																			 .statusCode(HttpStatus.CREATED.value())
																			 .build());
	}

	@PutMapping("/update")
	public ResponseEntity<Response> update(@Valid @RequestBody ProductDTO dto){
		Product product = ProductDTO.convertToEntity(dto);
		Product updated = service.update(product);
		ProductDTO productDTO = ProductDTO.convertToDto(updated);
		log.info("Produto atualizado com sucesso");
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(productDTO))
																			 .message(PRODUCT_RETRIEVED)
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	@DeleteMapping("/delete/id/{id}")
	public ResponseEntity<Response> delete(@PathVariable("id") Long id){
		service.delete(id);
		log.info("Produto deletado com sucesso");
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .message("Product deleted")
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	private ResponseEntity<Response> findProduct(Optional<Product> product){
		if(product.isEmpty()){
			log.warn("Produto não encontrado");
			return ResponseEntity.notFound()
													 .build();
		}
		ProductDTO productDTO = ProductDTO.convertToDto(product.get());
		log.info("Produto encontrado");
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(productDTO))
																			 .message(PRODUCT_RETRIEVED)
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	private ResponseEntity<Response> findProducts(List<Product> products){
		List<ProductDTO> productDTOS = new ArrayList<>();
		products.forEach(product->productDTOS.add(ProductDTO.convertToDto(product)));
		if(productDTOS.isEmpty()){
			log.warn("Produto não encontrados");
		}else{
			log.info("Produto encontrados");
		}
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(productDTOS))
																			 .message(PRODUCT_RETRIEVED)
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

}
