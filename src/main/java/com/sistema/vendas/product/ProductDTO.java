package com.sistema.vendas.product;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 18/12/2021
 */

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO{

	private Long id;

	private String name;

	private String brand;

	private String department;

	private String sector;

	private String section;

	private String weight;

	private LocalDate datePurchase;

	private Integer quantity;

	private Integer minimunQuantity;

	private Boolean hasASupplier;

	private Boolean hasACarrier;

	private BigDecimal costPrice;

	private BigDecimal sellingPrice;

	public static ProductDTO convertToDto(Product product){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(product, ProductDTO.class);
	}

	public static Product convertToEntity(ProductDTO dto){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(dto, Product.class);
	}

}
