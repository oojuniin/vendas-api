package com.sistema.vendas.address.city;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Data
@Entity(name = "cities")
@NoArgsConstructor
@AllArgsConstructor
public class City implements Serializable{

	private static final long serialVersionUID = -1328127571382428579L;

	@Id
	@SequenceGenerator(name = "city_sequence", sequenceName = "city_sequence", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "city_sequence")
	@Column(updatable = false)
	private Long id;

	@NotEmpty(message = "Digite um UF para a cidade")
	@Column(nullable = false)
	private String uf;

	@NotEmpty(message = "Digite um nome para a cidade")
	@Column(nullable = false)
	private String name;

}
