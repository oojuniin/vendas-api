package com.sistema.vendas.address.city;

import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

public interface CityService{

	Optional<City> findById(Long id);

	List<City> findByNameStartingWithIgnoreCase(String name);

	List<City> findByUfIgnoreCase(String uf);

	List<City> findAll();

	City save(City city);

	void delete(Long id);

}
