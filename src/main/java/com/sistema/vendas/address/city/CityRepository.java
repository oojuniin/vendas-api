package com.sistema.vendas.address.city;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Repository
public interface CityRepository extends JpaRepository<City, Long>{

	List<City> findByNameStartingWithIgnoreCase(String name);

	List<City> findByUfIgnoreCase(String uf);

}
