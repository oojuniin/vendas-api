package com.sistema.vendas.address;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Address implements Serializable{

	private static final long serialVersionUID = -1559149605015848708L;

	@NotEmpty(message = "Forneça um estado")
	@Column(nullable = false)
	@Size(max = 30)
	private String state;

	@NotEmpty(message = "forneça uma cidade")
	@Column(nullable = false)
	@Size(max = 30)
	private String city;

	@NotEmpty(message = "forneça um bairro")
	@Column(nullable = false)
	@Size(max = 30)
	private String district;

	@NotEmpty(message = "forneça o nome da rua")
	@Column(nullable = false)
	@Size(max = 30)
	private String street;

	@NotEmpty(message = "forneça um numero da casa")
	@Column(nullable = false)
	private String number;

	@NotEmpty(message = "forneça um cep")
	@Column(nullable = false)
	@Size(max = 10)
	private String cep;

}
