package com.sistema.vendas.address.state;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 25/11/2021
 */

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, timeout = 30)
public class StateServiceImpl implements StateService{

	private final StateRepository repository;

	@Override
	@Transactional(readOnly = true)
	public Optional<State> findById(Long id){
		return repository.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<State> findByNameStartingWithIgnoreCase(String name){
		return repository.findByNameStartingWithIgnoreCase(name);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<State> findByUfIgnoreCase(String uf){
		return repository.findByUfIgnoreCase(uf);
	}

	@Override
	@Transactional(readOnly = true)
	public List<State> findAll(){
		return repository.findAll();
	}

	@Override
	public State save(State state){
		return repository.save(state);
	}

	@Override
	public void delete(Long id){
		repository.deleteById(id);
	}

}
