package com.sistema.vendas.address.state;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Repository
public interface StateRepository extends JpaRepository<State, Long>{

	List<State> findByNameStartingWithIgnoreCase(String name);

	Optional<State> findByUfIgnoreCase(String uf);

}
