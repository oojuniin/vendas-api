package com.sistema.vendas.address.state;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sistema.vendas.response.Response;
import com.sistema.vendas.utils.Utils;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 25/11/2021
 */

@RestController
@RequestMapping("/api/v1/state")
@RequiredArgsConstructor
public class StateController{

	private final StateService service;

	@GetMapping("/find/id/{id}")
	public ResponseEntity<Response> findById(@PathVariable("id") Long id){
		return getResponse(service.findById(id));
	}

	@GetMapping("/find/uf/{uf}")
	public ResponseEntity<Response> findByUf(@PathVariable("uf") String uf){
		return getResponse(service.findByUfIgnoreCase(uf));
	}

	@GetMapping("/find/all")
	public ResponseEntity<Response> findAll(){
		return getResponse(service.findAll());
	}

	@GetMapping("/find/name/{name}")
	public ResponseEntity<Response> findByNameStartingWithIgnoreCase(@PathVariable("name") String name){
		return getResponse(service.findByNameStartingWithIgnoreCase(name));
	}

	private ResponseEntity<Response> getResponse(Optional<State> state){
		if(state.isEmpty()){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		StateDTO stateDto = StateDTO.convertToDto(state.get());
		return new ResponseEntity<>(Response.builder()
																				.timeStamp(Utils.formatDate())
																				.data(List.of(stateDto))
																				.message("Estado recuperado")
																				.status(HttpStatus.OK)
																				.statusCode(HttpStatus.OK.value())
																				.build(), HttpStatus.OK);
	}

	private ResponseEntity<Response> getResponse(List<State> states){
		List<StateDTO> statesDto = new ArrayList<>();
		states.forEach(state->statesDto.add(StateDTO.convertToDto(state)));
		return new ResponseEntity<>(Response.builder()
																				.timeStamp(Utils.formatDate())
																				.data(statesDto)
																				.message("Estados recuperados")
																				.status(HttpStatus.OK)
																				.statusCode(HttpStatus.OK.value())
																				.build(), HttpStatus.OK);
	}

}
