package com.sistema.vendas.address.state;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Data
@Entity(name = "states")
@NoArgsConstructor
@AllArgsConstructor
public class State implements Serializable{

	private static final long serialVersionUID = -6776933426008770787L;

	@Id
	@SequenceGenerator(name = "state_sequence", sequenceName = "state_sequence", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "state_sequence")
	@Column(updatable = false)
	private Long id;

	@NotEmpty(message = "Digite um UF para a cidade")
	@Column(nullable = false)
	private String uf;

	@NotEmpty(message = "Digite um nome para o estado")
	@Column(nullable = false)
	private String name;

}
