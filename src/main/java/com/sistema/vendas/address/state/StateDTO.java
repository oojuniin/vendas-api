package com.sistema.vendas.address.state;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 27/11/2021
 */

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class StateDTO{

	private Long id;
	private String name;
	private String uf;

	public static State convertToEntity(StateDTO dto){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(dto, State.class);
	}

	public static StateDTO convertToDto(State city){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(city, StateDTO.class);
	}

}
