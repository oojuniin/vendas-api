package com.sistema.vendas.response;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 28/11/2021
 */

@Getter
@SuperBuilder
@JsonInclude(NON_NULL)
public class Response{

	private String timeStamp;
	private int statusCode;
	private HttpStatus status;
	private String reason;
	private String message;
	private String developMessage;
	private List<?> data;

}
