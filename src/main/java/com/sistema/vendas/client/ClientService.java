package com.sistema.vendas.client;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 05/12/2021
 */

public interface ClientService{

	Page<Client> findAll(int page, int sizer);

	List<Client> findAll();

	Optional<Client> findById(Long id);

	Client save(Client client);

	Client update(Client client);

	void delete(Long id);

	List<Client> findByPersonalData_NameStartingWithIgnoreCase(String name);

	List<Client> findByPersonalData_CpfStartingWith(String cpf);

}
