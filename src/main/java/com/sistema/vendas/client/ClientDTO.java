package com.sistema.vendas.client;

import com.sistema.vendas.personal.PersonalData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDate;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 05/12/2021
 */

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO extends RepresentationModel<ClientDTO>{

	private Long id;
	private PersonalData personalData;

	public static ClientDTO convertToDto(Client client){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(client, ClientDTO.class);
	}

	public static Client convertToEntity(ClientDTO dto){
		ModelMapper modelMapper = new ModelMapper();
		dto.getPersonalData()
			 .setRegistrationDate(LocalDate.now());
		return modelMapper.map(dto, Client.class);
	}

	public Long getId(){
		return id;
	}

}
