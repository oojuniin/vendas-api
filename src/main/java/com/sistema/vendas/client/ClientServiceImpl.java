package com.sistema.vendas.client;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 05/12/2021
 */

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, timeout = 30)
public class ClientServiceImpl implements ClientService{

	private final ClientRepository repository;

	@Override
	@Transactional(readOnly = true)
	public Page<Client> findAll(int page, int size){
		Pageable elementsForPage = PageRequest.of(page, size);
		return repository.findAll(elementsForPage);
	}

	@Override
	public List<Client> findAll(){
		return repository.findAll(Sort.by(Sort.Direction.ASC, "personalData.name"));
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Client> findById(Long id){
		return repository.findById(id);
	}

	@Override
	public Client save(Client client){
		return repository.save(client);
	}

	@Override
	public Client update(Client client){
		return repository.save(client);
	}

	@Override
	public void delete(Long id){
		repository.deleteById(id);
	}

	@Override
	public List<Client> findByPersonalData_NameStartingWithIgnoreCase(String name){
		return repository.findByPersonalData_NameStartingWithIgnoreCase(name);
	}

	@Override
	public List<Client> findByPersonalData_CpfStartingWith(String cpf){
		return repository.findByPersonalData_CpfStartingWith(cpf);
	}

}
