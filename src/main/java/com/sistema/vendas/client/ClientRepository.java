package com.sistema.vendas.client;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 05/12/2021
 */

@Repository
public interface ClientRepository extends JpaRepository<Client, Long>{

	List<Client> findByPersonalData_NameStartingWithIgnoreCase(String name);

	List<Client> findByPersonalData_CpfStartingWith(String cpf);

}
