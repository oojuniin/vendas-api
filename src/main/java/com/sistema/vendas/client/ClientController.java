package com.sistema.vendas.client;

import com.sistema.vendas.response.Response;
import com.sistema.vendas.utils.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 05/12/2021
 */

@RestController
@RequestMapping("/api/v1/client")
@RequiredArgsConstructor
public class ClientController{

	private final ClientService service;
	private static final String CLIENTS_RETRIEVED = "Clientes recuperados";
	private static final String CLIENT_RETRIEVED = "Cliente recuperado";

	@GetMapping("/find/all")
	public ResponseEntity<Response> findAll(){
		return getResponse(service.findAll());
	}

	@GetMapping(value = "/find/all", params = "page")
	public ResponseEntity<Response> findAll(@RequestParam("page") int page){
		Page<Client> clients = service.findAll(page, 5);
		List<ClientDTO> clientDTOS = new ArrayList<>();
		clients.forEach(client->clientDTOS.add(ClientDTO.convertToDto(client)));
		clientDTOS.forEach(dto->dto.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(ClientController.class)
																																							.findById(dto.getId()))
																										 .withSelfRel()));
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(clientDTOS)
																			 .message(CLIENTS_RETRIEVED)
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	@GetMapping("find/name/{name}")
	public ResponseEntity<Response> findByName(@PathVariable("name") String name){
		return getResponse(service.findByPersonalData_NameStartingWithIgnoreCase(name));
	}

	@GetMapping("find/cpf/{cpf}")
	public ResponseEntity<Response> findByCpf(@PathVariable("cpf") String cpf){
		return getResponse(service.findByPersonalData_CpfStartingWith(cpf));
	}

	@GetMapping("/find/id/{id}")
	public ResponseEntity<Response> findById(@PathVariable("id") Long id){
		return getResponse(service.findById(id));
	}

	@PostMapping("/save")
	public ResponseEntity<Response> save(@RequestBody ClientDTO dto){
		Client client = ClientDTO.convertToEntity(dto);
		Client save = service.save(client);
		ClientDTO clientDTO = ClientDTO.convertToDto(save);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
																							.path("/api/v1/client/find/id/{id}")
																							.buildAndExpand(clientDTO.getId())
																							.toUri();
		return ResponseEntity.created(location)
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(clientDTO))
																			 .message("Cliente Adicionado")
																			 .status(CREATED)
																			 .statusCode(CREATED.value())
																			 .build());
	}

	@PutMapping("/update")
	public ResponseEntity<Response> update(@Valid @RequestBody ClientDTO dto){
		Client client = ClientDTO.convertToEntity(dto);
		Client save = service.update(client);
		ClientDTO clientDTO = ClientDTO.convertToDto(save);
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(clientDTO))
																			 .message("Cliente atualizado")
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	@DeleteMapping("/delete/id/{id}")
	public ResponseEntity<Response> delete(@PathVariable("id") Long id){
		service.delete(id);
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .message("Cliente deletado")
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	private ResponseEntity<Response> getResponse(Optional<Client> client){
		if(client.isEmpty()){
			return ResponseEntity.notFound()
													 .build();
		}
		ClientDTO clientDTO = ClientDTO.convertToDto(client.get());
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(clientDTO))
																			 .message(CLIENT_RETRIEVED)
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	private ResponseEntity<Response> getResponse(List<Client> clients){
		List<ClientDTO> clientDTOS = new ArrayList<>();
		clients.forEach(client->clientDTOS.add(ClientDTO.convertToDto(client)));
		clientDTOS.forEach(dto->dto.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(ClientController.class)
																																							.findById(dto.getId()))
																										 .withSelfRel()));
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(clientDTOS)
																			 .message(CLIENTS_RETRIEVED)
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

}
