package com.sistema.vendas.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Configuration
public class CorsConfig implements WebMvcConfigurer{

	@Override
	public void addCorsMappings(CorsRegistry registry){
		registry.addMapping("/**")
						.allowedMethods("GET", "POST", "PUT", "DELETE");
	}

}
