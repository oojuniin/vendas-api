package com.sistema.vendas.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

import io.swagger.models.auth.In;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 25/11/2021
 */

@Configuration
public class SwaggerConfig{

	@Bean
	public Docket vendasApi(){
		return new Docket(DocumentationType.SWAGGER_2).select()
																									.apis(RequestHandlerSelectors.basePackage("com.sistema.vendas"))
																									.paths(PathSelectors.ant("/api/**"))
																									.build()
																									.securitySchemes(Arrays.asList(
																											new ApiKey("Token Access", HttpHeaders.AUTHORIZATION,
																													In.HEADER.name())))
																									.useDefaultResponseMessages(false)
																									.apiInfo(apiInfo());
	}

	private ApiInfo apiInfo(){
		return new ApiInfoBuilder().title("API Sistema de vendas")
															 .description("Uma API simples que para um sistema de vendas online")
															 .version("1.0.0")
															 .license("Apache License Version 2.0")
															 .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
															 .contact(new Contact("Junior Lima", "https://www.linkedin.com/in/oojuniin/",
																	 "juniiorliimatt@gmail.com"))
															 .build();
	}

	List<SecurityReference> defaultAuth(){
		AuthorizationScope authorizationScope = new AuthorizationScope("ADMIN", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return Arrays.asList(new SecurityReference("Token Access", authorizationScopes));
	}

	@Bean
	public UiConfiguration uiConfig(){
		return UiConfigurationBuilder.builder()
																 .deepLinking(true)
																 .displayOperationId(false)
																 .defaultModelsExpandDepth(1)
																 .defaultModelExpandDepth(1)
																 .defaultModelRendering(ModelRendering.EXAMPLE)
																 .displayRequestDuration(false)
																 .docExpansion(DocExpansion.NONE)
																 .filter(false)
																 .maxDisplayedTags(null)
																 .operationsSorter(OperationsSorter.ALPHA)
																 .showExtensions(false)
																 .showCommonExtensions(false)
																 .tagsSorter(TagsSorter.ALPHA)
																 .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
																 .validatorUrl(null)
																 .build();
	}

}
