package com.sistema.vendas.config;

import com.sistema.vendas.filter.CustomAuthenticationFilter;
import com.sistema.vendas.filter.CustomAuthorizationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.Arrays;
import java.util.List;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Value(value = "${API_SECRET}")
	private String secret;

	@Value(value = "${API_EXPIRATIONS_MS}")
	private String expiration;

	@Value("${spring.profiles.active}")
	private String activeProfile;

	private final UserDetailsService userDetailsService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
		auth.userDetailsService(userDetailsService)
				.passwordEncoder(bCryptPasswordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception{
		CustomAuthenticationFilter authenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean(),
				secret, expiration);
		authenticationFilter.setFilterProcessesUrl("/api/login");

		final String ROLE_ADMIN = "ROLE_ADMIN";
		final String URL_USER = "/api/v1/user/**";
		final String URL_ROLE = "/api/v1/role/**";
		final String URL_EMPLOYEE = "/api/v1/employee/**";

		final List<String> urlsGet = Arrays.asList(URL_USER, URL_ROLE);
		final List<String> urlsPost = Arrays.asList(URL_USER, URL_ROLE, URL_EMPLOYEE);
		final List<String> urlsPut = Arrays.asList(URL_USER, URL_ROLE, URL_EMPLOYEE);
		final List<String> urlsDelete = Arrays.asList(URL_USER, URL_ROLE, URL_EMPLOYEE);

		http.csrf()
				.disable();

		http.cors();

		if(activeProfile.equals("test")){
			http.authorizeRequests()
					.anyRequest()
					.permitAll();
		}else{
			http.sessionManagement()
					.sessionCreationPolicy(STATELESS);

			// Urls
			http.authorizeRequests()
					.antMatchers(GET, String.valueOf(urlsGet))
					.hasAnyAuthority(ROLE_ADMIN);

			http.authorizeRequests()
					.antMatchers(POST, String.valueOf(urlsPost))
					.hasAnyAuthority(ROLE_ADMIN);

			http.authorizeRequests()
					.antMatchers(PUT, String.valueOf(urlsPut))
					.hasAnyAuthority(ROLE_ADMIN);

			http.authorizeRequests()
					.antMatchers(DELETE, String.valueOf(urlsDelete))
					.hasAnyAuthority(ROLE_ADMIN);

			// Refresh token
			http.authorizeRequests()
					.antMatchers("/api/login/")
					.permitAll();

			// Actuator
			http.authorizeRequests()
					.antMatchers(GET, "/actuator/**")
					.permitAll();

			// Swagger
			http.authorizeRequests()
					.antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security",
							"/swagger-ui.html", "/ webjars/**", "/swagger-resources/configuration/ui",
							"/swagger-resources/configuration/security", "/configuration/**", "/swagger-resources/**",
							"/swagger-ui/**")
					.permitAll();

			http.authorizeRequests()
					.anyRequest()
					.authenticated()/*.and().oauth2ResourceServer().jwt()*/;

			http.addFilter(authenticationFilter);

			http.addFilterBefore(new CustomAuthorizationFilter(secret), UsernamePasswordAuthenticationFilter.class);
		}
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception{
		return super.authenticationManagerBean();
	}

}
