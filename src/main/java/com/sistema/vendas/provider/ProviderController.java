package com.sistema.vendas.provider;

import static org.springframework.http.HttpStatus.OK;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sistema.vendas.response.Response;
import com.sistema.vendas.utils.Utils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 07/12/2021
 */

@Slf4j
@RestController
@RequestMapping("/api/v1/provider")
@RequiredArgsConstructor
public class ProviderController{

	private final ProviderService service;
	private static final String PROVIDER_RETRIEVED = "Provider retrieved";

	@GetMapping("/find/all")
	public ResponseEntity<Response> findAll(){
		List<Provider> providers = service.findAll();
		return findProviders(providers);
	}

	@GetMapping("/find/id/{id}")
	public ResponseEntity<Response> findById(@PathVariable("id") Long id){
		Optional<Provider> provider = service.findById(id);
		return findProvider(provider);
	}

	@PostMapping("/save")
	public ResponseEntity<Response> save(@RequestBody ProviderDTO dto){
		Provider provider = ProviderDTO.convertToEntity(dto);
		Provider save = service.save(provider);
		ProviderDTO providerDTO = ProviderDTO.convertToDto(save);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
																							.path("/api/v1/provider/find/id/{id}")
																							.buildAndExpand(providerDTO.getId())
																							.toUri();
		log.info("Fornecedor {} salvo.", providerDTO.getCorporateName());
		return ResponseEntity.created(location)
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(providerDTO))
																			 .message(PROVIDER_RETRIEVED)
																			 .status(HttpStatus.CREATED)
																			 .statusCode(HttpStatus.CREATED.value())
																			 .build());
	}

	@PutMapping("/update")
	public ResponseEntity<Response> update(@RequestBody ProviderDTO dto){
		Provider provider = ProviderDTO.convertToEntity(dto);
		Provider updated = service.save(provider);
		ProviderDTO providerDTO = ProviderDTO.convertToDto(updated);
		log.info("Fornecedor atualizado");
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(providerDTO))
																			 .message(PROVIDER_RETRIEVED)
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	@DeleteMapping("/delete/id/{id}")
	public ResponseEntity<Response> delete(@PathVariable("id") Long id){
		service.delete(id);
		log.info("Fornecedor deletado");
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .message("Provider deleted")
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	private ResponseEntity<Response> findProvider(Optional<Provider> provider){
		if(provider.isEmpty()){
			return ResponseEntity.notFound()
													 .build();
		}
		ProviderDTO providerDTO = ProviderDTO.convertToDto(provider.get());
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(providerDTO))
																			 .message(PROVIDER_RETRIEVED)
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	private ResponseEntity<Response> findProviders(List<Provider> providers){
		List<ProviderDTO> providerDTOS = new ArrayList<>();
		providers.forEach(provider->providerDTOS.add(ProviderDTO.convertToDto(provider)));
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(providerDTOS))
																			 .message(PROVIDER_RETRIEVED)
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

}
