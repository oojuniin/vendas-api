package com.sistema.vendas.provider;

import java.time.LocalDate;

import org.modelmapper.ModelMapper;

import com.sistema.vendas.address.Address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 07/12/2021
 */

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class ProviderDTO{

	private Long id;
	private String corporateName;
	private String fantasyName;
	private String cnpj;
	private String checkingAccount;
	private String representative;
	private String enrollment;
	private LocalDate foundationDate;
	private String comments;
	private String email;
	private String phoneNumberOne;
	private String phoneNumberTwo;
	private Address address;

	public static ProviderDTO convertToDto(Provider provider){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(provider, ProviderDTO.class);
	}

	public static Provider convertToEntity(ProviderDTO dto){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(dto, Provider.class);
	}

}
