package com.sistema.vendas.provider;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 07/12/2021
 */

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Long>{

}
