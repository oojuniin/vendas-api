package com.sistema.vendas.personal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sistema.vendas.address.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 27/11/2021
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class PersonalData implements Serializable{

	private static final long serialVersionUID = -5173137153297393669L;

	@NotEmpty(message = "Insira um nome")
	@Column(nullable = false)
	private String name;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(nullable = false)
	private LocalDate birthday;

	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(nullable = false)
	private LocalDate registrationDate;

	@CPF
	@NotEmpty(message = "Insira um CPF válido")
	@Column(unique = true, nullable = false)
	private String cpf;

	@NotEmpty(message = "Insira um RG válido")
	@Size(max = 13)
	@Column(unique = true, nullable = false)
	private String rg;

	@Email
	@NotEmpty(message = "Insira um email")
	@Column(unique = true)
	private String email;

	@Embedded
	@Valid
	@Cascade(CascadeType.ALL)
	private Address address;

	@NotEmpty(message = "Insira um número válido")
	@Column(unique = true, nullable = false)
	private String phoneNumberOne;

	private String phoneNumberTwo;

	private String comments;

}
