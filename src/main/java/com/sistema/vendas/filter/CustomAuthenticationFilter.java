package com.sistema.vendas.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Slf4j
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter{

	private final AuthenticationManager manager;
	private final String secret;
	private final String expiration;

	public CustomAuthenticationFilter(AuthenticationManager manager, String secret, String expiration){
		this.manager = manager;
		this.secret = secret;
		this.expiration = expiration;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException{
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
		return manager.authenticate(token);
	}

	@Override
	public void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException{
		User user = (User) authentication.getPrincipal();
		Algorithm algorithm = Algorithm.HMAC256(secret.getBytes());
		Date today = new Date();
		Date expirationDate = new Date(today.getTime() + Long.parseLong(expiration));

		String accessToken = JWT.create()
														.withSubject(user.getUsername())
														.withExpiresAt(expirationDate)
														.withIssuer(request.getRequestURL()
																							 .toString())
														.withClaim("roles", user.getAuthorities()
																										.stream()
																										.map(GrantedAuthority::getAuthority)
																										.collect(Collectors.toList()))
														.sign(algorithm);

		Map<String, String> token = new HashMap<>();
		token.put("user", user.toString());
		token.put("access_token", accessToken);
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		new ObjectMapper().writeValue(response.getOutputStream(), token);
	}

}
