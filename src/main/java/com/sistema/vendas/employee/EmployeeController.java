package com.sistema.vendas.employee;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sistema.vendas.response.Response;
import com.sistema.vendas.utils.Utils;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 29/11/2021
 */

@RestController
@RequestMapping("/api/v1/employee")
@RequiredArgsConstructor
public class EmployeeController{

	private final EmployeeService service;
	private static final String EMPLOYEES_RETRIEVED = "Funcionários recuperados";
	private static final String EMPLOYEE = "Funcionário Recuperado";

	// TODO implementar documentação para o swagger

	@ApiResponses(value = {@ApiResponse(code = 200, message = "Retorna a lista de pessoa"), @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"), @ApiResponse(code = 500, message = "Foi gerada uma exceção")})
	@ApiOperation(value = "Recuperar todos os funcionários.")
	@GetMapping(value = "/find/all")
	public ResponseEntity<Response> findAll(){
		List<EmployeeData> employees = service.findAll();
		return getResponse(employees);
	}

	@ApiOperation(value = "Encontra um funcionário pelo ID")
	@GetMapping("/find/id/{id}")
	public ResponseEntity<Response> findById(@PathVariable("id") Long id){
		return getResponse(service.findById(id));
	}

	@GetMapping("/find/name/{name}")
	public ResponseEntity<Response> findByPersonalData_NameStartingWithIgnoreCase(@PathVariable("name") String name){
		return getResponse(service.findByPersonalData_NameStartingWithIgnoreCase(name));
	}

	@GetMapping("/find/cpf/{cpf}")
	public ResponseEntity<Response> findByPersonalData_CpfLike(@PathVariable("cpf") String cpf){
		return getResponse(service.findByPersonalData_CpfLike(cpf));
	}

	@PostMapping("/save")
	public ResponseEntity<Response> save(@Valid @RequestBody EmployeeDTO dto){
		EmployeeData employee = EmployeeDTO.convertToEntity(dto);
		EmployeeData save = service.save(employee);
		EmployeeDTO employeeDTO = EmployeeDTO.convertToDto(save);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
																							.path("/api/v1/employee/find/id/{id}")
																							.buildAndExpand(employeeDTO.getId())
																							.toUri();
		return ResponseEntity.created(location)
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(employeeDTO))
																			 .message(EMPLOYEE)
																			 .status(HttpStatus.CREATED)
																			 .statusCode(HttpStatus.CREATED.value())
																			 .build());
	}

	@PutMapping("/update")
	public ResponseEntity<Response> update(@Valid @RequestBody EmployeeDTO dto){
		EmployeeData employeeData = EmployeeDTO.convertToEntity(dto);
		EmployeeData updated = service.update(employeeData);
		EmployeeDTO employeeDTO = EmployeeDTO.convertToDto(updated);
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(employeeDTO))
																			 .message(EMPLOYEE)
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	@DeleteMapping("/delete/id/{id}")
	public ResponseEntity<Response> delete(@PathVariable("id") Long id){
		service.delete(id);
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .message("Funcionário deletado")
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	private ResponseEntity<Response> getResponse(Optional<EmployeeData> employeeData){
		if(employeeData.isEmpty()){
			return getResponse();
		}
		EmployeeDTO employeeDTO = EmployeeDTO.convertToDto(employeeData.get());
		return new ResponseEntity<>(Response.builder()
																				.timeStamp(Utils.formatDate())
																				.data(List.of(employeeDTO))
																				.message(EMPLOYEE)
																				.status(HttpStatus.OK)
																				.statusCode(HttpStatus.OK.value())
																				.build(), HttpStatus.OK);

	}

	private ResponseEntity<Response> getResponse(){
		return new ResponseEntity<>(Response.builder()
																				.timeStamp(Utils.formatDate())
																				.message("Funcionário não encontrado")
																				.status(HttpStatus.NOT_FOUND)
																				.statusCode(HttpStatus.NOT_FOUND.value())
																				.build(), HttpStatus.NOT_FOUND);
	}

	private ResponseEntity<Response> getResponse(List<EmployeeData> employees){
		List<EmployeeDTO> employeeDTOS = new ArrayList<>();
		employees.forEach(employee->employeeDTOS.add(EmployeeDTO.convertToDto(employee)));
		return new ResponseEntity<>(Response.builder()
																				.timeStamp(Utils.formatDate())
																				.data(List.of(employeeDTOS))
																				.message(EMPLOYEES_RETRIEVED)
																				.status(HttpStatus.OK)
																				.statusCode(HttpStatus.OK.value())
																				.build(), HttpStatus.OK);
	}

}
