package com.sistema.vendas.employee;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sistema.vendas.user.User;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 29/11/2021
 */

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, timeout = 30)
public class EmployeeServiceImpl implements EmployeeService{

	private final EmployeeRepository repository;
	private final PasswordEncoder encoder;

	@Override
	@Transactional(readOnly = true)
	public List<EmployeeData> findAll(){
		return repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<EmployeeData> findById(Long id){
		return repository.findById(id);
	}

	@Override
	public EmployeeData save(EmployeeData employeeData){
		String password = encoder.encode(employeeData.getUser()
																								 .getPassword());
		User user = new User(null, employeeData.getUser()
																					 .getUsername(), password, null);
		EmployeeData employee = new EmployeeData(null, employeeData.getHiringDate(), employeeData.getWorkPermit(),
				employeeData.getSalePercentage(), employeeData.getCashBalance(), employeeData.getSalary(),
				employeeData.getOffice(), employeeData.getPersonalData(), user);
		return repository.save(employee);
	}

	@Override
	public EmployeeData update(EmployeeData employeeData){
		return repository.save(employeeData);
	}

	@Override
	public void delete(Long id){
		repository.deleteById(id);
	}

	@Override
	public List<EmployeeData> findByPersonalData_NameStartingWithIgnoreCase(String name){
		return repository.findByPersonalData_NameStartingWithIgnoreCase(name);
	}

	@Override
	public Optional<EmployeeData> findByPersonalData_CpfLike(String cpf){
		Optional<EmployeeData> employeeData = repository.findByPersonalData_CpfLike(cpf);
		if(employeeData.isEmpty()){
			return Optional.empty();
		}
		return employeeData;
	}

	@Override
	public List<EmployeeData> findByPersonalData_Birthday(LocalDate birthday){
		// TODO implementar busca por data de aniversário
		return repository.findByPersonalData_Birthday(birthday);
	}

	@Override
	public List<EmployeeData> findByPersonalData_RegistrationDate(LocalDate registrationDate){
		// TODO implementar busca por data de registro
		return repository.findByPersonalData_RegistrationDate(registrationDate);
	}

	@Override
	public Optional<EmployeeData> findByPersonalData_Rg(String rg){
		// TODO implementar busca por RG
		return repository.findByPersonalData_Rg(rg);
	}

	@Override
	public List<EmployeeData> findByHiringDate(LocalDate hiringDate){
		// TODO implementar busca por data de contratação
		return repository.findByHiringDate(hiringDate);
	}

	@Override
	public List<EmployeeData> findByCashBalanceOrderByCashBalanceDesc(BigDecimal cashBalance){
		// TODO implementar busca por cash
		return repository.findByCashBalanceOrderByCashBalanceDesc(cashBalance);
	}

}
