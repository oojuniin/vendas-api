package com.sistema.vendas.role;

import static javax.persistence.GenerationType.SEQUENCE;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Data
@Entity(name = "roles")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Role implements GrantedAuthority{

	private static final long serialVersionUID = 800004466311496913L;

	@Id
	@SequenceGenerator(name = "role_sequence", sequenceName = "role_sequence", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "role_sequence")
	@Column(updatable = false)
	private Long id;

	@Column(unique = true)
	private String name;

	@Override
	public String getAuthority(){
		return this.name;
	}

}
