package com.sistema.vendas.role;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sistema.vendas.response.Response;
import com.sistema.vendas.utils.Utils;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@RestController
@RequestMapping("/api/v1/role")
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class RoleController{

	private final RoleService service;

	// TODO Refatorar

	@PostMapping("/save")
	public ResponseEntity<Response> saveRole(@RequestBody RoleDTO dto){
		Role role = RoleDTO.convertToEntity(dto);
		Role save = service.save(role);
		RoleDTO roleDto = RoleDTO.convertToDto(save);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
																							.path("/api/v1/role/find/id/{id}")
																							.buildAndExpand(roleDto.getId())
																							.toUri();
		return ResponseEntity.created(location)
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(roleDto))
																			 .message("Role salva")
																			 .status(HttpStatus.CREATED)
																			 .statusCode(HttpStatus.CREATED.value())
																			 .build());
	}

	@GetMapping("/find/all")
	public ResponseEntity<Response> findAll(){
		List<Role> roles = service.findAll();

		if(roles.isEmpty()){
			return ResponseEntity.noContent()
													 .build();
		}

		List<RoleDTO> roleDTOS = new ArrayList<>();
		roles.forEach(role->roleDTOS.add(RoleDTO.convertToDto(role)));
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(roleDTOS))
																			 .message("Roles recuperadas")
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	@GetMapping("/find/id/{id}")
	public ResponseEntity<Response> findById(@PathVariable("id") Long id){
		Optional<Role> role = service.findById(id);
		if(role.isEmpty()){
			return ResponseEntity.notFound()
													 .build();
		}
		RoleDTO roleDto = RoleDTO.convertToDto(role.get());
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(roleDto))
																			 .message("Role recuperada")
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	@DeleteMapping("/delete/id/{id}")
	public ResponseEntity<Response> deleteById(@PathVariable("id") Long id){
		Optional<Role> roleOptional = service.delete(id);
		if(roleOptional.isEmpty()){
			return new ResponseEntity<>(Response.builder()
																					.timeStamp(Utils.formatDate())
																					.message("Role não encontrada")
																					.status(HttpStatus.NOT_FOUND)
																					.statusCode(HttpStatus.NOT_FOUND.value())
																					.build(), HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .message("Role deletada")
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

}
