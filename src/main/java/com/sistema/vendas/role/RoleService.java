package com.sistema.vendas.role;

import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

public interface RoleService{

	List<Role> findAll();

	Optional<Role> findById(Long id);

	Role save(Role role);

	Role update(Role role);

	Optional<Role> delete(Long id);

}
