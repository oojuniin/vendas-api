package com.sistema.vendas.user;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import com.sistema.vendas.role.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO{

	private Long id;
	private String username;
	private String password;
	private List<Role> roles = new ArrayList<>();

	public UserDTO(User user){
		this.id = user.getId();
		this.username = user.getUsername();
		this.password = user.getPassword();
		roles.addAll(user.getRoles());
	}

	public static User convertToEntity(UserDTO dto){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(dto, User.class);
	}

	public static UserDTO convertToDto(User user){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(user, UserDTO.class);
	}

}
