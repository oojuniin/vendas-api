package com.sistema.vendas.user;

import lombok.Data;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Data
public class RoleToUserForm{

	private String username;
	private String roleName;

}
