package com.sistema.vendas.user;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sistema.vendas.exception.RefreshTokenMissingException;
import com.sistema.vendas.exception.RoleNotFoundException;
import com.sistema.vendas.role.Role;
import com.sistema.vendas.role.RoleRepository;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, isolation = Isolation.SERIALIZABLE, timeout = 30)
public class UserServiceImpl implements UserService, UserDetailsService{

	private final UserRepository repository;
	private final RoleRepository roleRepository;
	private final PasswordEncoder encoder;

	@Value("${API_EXPIRATIONS_MS}")
	private String expiration;

	@Value("${API_SECRET}")
	private String secret;

	@Override
	public User save(User user){
		if(user.getPassword()
					 .length()<25){
			User usr = new User(user.getId(), user.getUsername(), encoder.encode(user.getPassword()));
			return repository.save(usr);
		}
		return repository.save(user);
	}

	@Override
	public void addRoleToUser(String username, String roleName) throws RoleNotFoundException{
		Optional<User> user = repository.findByUsername(username);
		if(user.isEmpty()){
			throw new UsernameNotFoundException("user not found");
		}

		Optional<Role> role = roleRepository.findByName(roleName);

		if(role.isEmpty()){
			throw new RoleNotFoundException("Role não encontrada");
		}
		user.get()
				.getRoles()
				.add(role.get());
		repository.save(user.get());
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<User> findByUsername(String username){
		return repository.findByUsername(username);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<User> findById(Long id){
		return repository.findById(id);
	}

	@Override
	public List<User> findByUsernameStartingWith(String username){
		return repository.findByUsernameStartingWith(username);
	}

	@Override
	@Transactional(readOnly = true)
	public List<User> findAll(){
		return repository.findAll();
	}

	@Override
	public void delete(Long id){
		repository.deleteById(id);
	}

	@Override
	public User update(User user){
		return repository.save(user);
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
		Optional<User> user = repository.findByUsername(username);
		if(user.isEmpty()){
			throw new UsernameNotFoundException("User not found in the database");
		}

		User u = user.get();
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
		u.getRoles()
		 .forEach(role->authorities.add(new SimpleGrantedAuthority(role.getName())));
		return new org.springframework.security.core.userdetails.User(u.getUsername(), u.getPassword(), authorities);
	}

	@Override
	public Map<String, String> refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException, RefreshTokenMissingException{
		String authorizationHeader = request.getHeader(AUTHORIZATION);
		Date today = new Date();
		Date expirationDate = new Date(today.getTime() + Long.parseLong(expiration));

		if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
			try{
				String refreshToken = authorizationHeader.substring("Bearer ".length());
				Algorithm algorithm = Algorithm.HMAC256(secret.getBytes());
				JWTVerifier verifier = JWT.require(algorithm)
																	.build();
				DecodedJWT decodedJWT = verifier.verify(refreshToken);
				String username = decodedJWT.getSubject();
				Optional<User> user = this.findByUsername(username);
				if(user.isPresent()){
					User u = user.get();
					String accessToken = JWT.create()
																	.withSubject(u.getUsername())
																	.withExpiresAt(expirationDate)
																	.withIssuer(request.getRequestURL()
																										 .toString())
																	.withClaim("roles", u.getRoles()
																											 .stream()
																											 .map(Role::getName)
																											 .collect(Collectors.toList()))
																	.sign(algorithm);
					Map<String, String> tokens = new HashMap<>();
					tokens.put("access_token", accessToken);
					response.setContentType(MediaType.APPLICATION_JSON_VALUE);
					return tokens;
				}
			}catch(Exception exception){
				response.setStatus(FORBIDDEN.value());
				Map<String, String> error = new HashMap<>();
				error.put("error_message", exception.getMessage());
				response.setContentType(MediaType.APPLICATION_JSON_VALUE);
				new ObjectMapper().writeValue(response.getOutputStream(), error);
			}
		}else{
			throw new RefreshTokenMissingException("Token inválido");
		}
		return Collections.emptyMap();
	}

}
