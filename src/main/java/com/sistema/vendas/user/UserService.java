package com.sistema.vendas.user;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.management.relation.RoleNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

public interface UserService{

	User save(User user);

	void addRoleToUser(String username, String roleName) throws RoleNotFoundException;

	Optional<User> findByUsername(String username);

	Optional<User> findById(Long id);

	List<User> findByUsernameStartingWith(String username);

	List<User> findAll();

	void delete(Long id);

	User update(User user);

	Map<String, String> refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException;

}
